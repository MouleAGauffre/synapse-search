package service

import java.util.stream.Collectors
import java.util.stream.Stream
import model.KnowledgeGraph
import model.Node
import kotlin.streams.toList

class Neighbourhood {

    private var aggregators: MutableList<Aggregator> = mutableListOf()

    fun compute(nodeID: String, graph: KnowledgeGraph, maxDistance: Float): Map<Node, Float> {
        val results: HashMap<Node, Float> = hashMapOf()
        val n0 = graph.nodes[nodeID]!!
        aggregators.add(Aggregator(n0, hashMapOf(n0 to 0f), 0f))

        while (aggregators.isNotEmpty()) {
            val newAggregators = aggregators.parallelStream()
                .flatMap { processMessage(it, maxDistance) }
                .collect(Collectors.partitioningBy { it.willPropagate(maxDistance) })

            aggregators = newAggregators[true]!!
            results.putAll(fetchResultsFromCompletedAggregators(newAggregators[false]!!))
        }

        return results
    }

    private fun processMessage(msg: Aggregator, maxDistance: Float): Stream<Aggregator> =
        when (msg.willPropagate(maxDistance)) {
            true -> msg.destination.neighbours.entries.parallelStream()
                .map { Aggregator.create(msg, it.key, it.value) }
            false -> Stream.of()
        }

    private fun fetchResultsFromCompletedAggregators(completedAggregators: List<Aggregator>): Map<Node, Float> =
        completedAggregators.parallelStream()
            .flatMap { it.payload.entries.parallelStream() }
            .toList()
            .associate { it.key to it.value }

}