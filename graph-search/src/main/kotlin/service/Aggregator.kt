package service

import model.Node

class Aggregator(
    val destination: Node,
    val payload: HashMap<Node, Float>,
    val distance: Float,
) {

    fun willPropagate(maxDistance: Float): Boolean =
        distance < maxDistance || (payload.containsKey(destination) && distance < payload[destination]!!)

    companion object Factory {
        fun create(previousMsg: Aggregator, edgeLength: Float, destination: Node): Aggregator {
            val newPayload: HashMap<Node, Float> = HashMap(previousMsg.payload)
            val newDistance = previousMsg.distance + edgeLength
            newPayload[destination] = newDistance
            return Aggregator(destination, newPayload, newDistance)
        }
    }

}