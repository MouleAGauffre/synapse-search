package resource

import javax.ws.rs.Path
import model.KnowledgeGraph
import service.Neighbourhood
import kotlin.streams.toList

@Path("/")
class GraphSearchResource(
    private val graph: KnowledgeGraph = KnowledgeGraph(),
    private val neighbourhood: Neighbourhood = Neighbourhood()
) {

    @Path("/search")
    fun search(ids: List<String>): Map<Float, String> = ids
        .flatMap { neighbourhood.compute(it, graph, System.getenv("NEIGHBOURHOOD_RADIUS").toFloat()).entries }
        .groupBy { it.key }
        .entries.parallelStream()
        .map { it.key to 1f / it.value.map { entry -> 1f / entry.value }.sum() }
        .toList()
        .sortedBy { it.second }
        .associate { it.second to it.first.id }

    @Path("/loadGraph")
    fun loadGraph() {
    }

}