package model

data class KnowledgeGraph(val nodes: HashMap<String, Node> = hashMapOf())