package model

data class Node(
    val id: String,
    val neighbours: HashMap<Float, Node> = hashMapOf()
)