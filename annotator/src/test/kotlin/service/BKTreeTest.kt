package service

import io.quarkus.test.junit.QuarkusTest
import java.util.*
import org.apache.commons.text.similarity.LevenshteinDistance
import org.junit.Assert
import org.junit.Test

@QuarkusTest
class BKTreeTest {

    @Test
    fun shouldIndexAndSearch() {
        val tree = BKTree(LevenshteinDistance())
        val dictionary = listOf("help", "hell", "hello", "loop", "help", "shell", "helper", "troop")

        dictionary.forEach { tree.index(it, UUID.randomUUID().toString()) }

        Assert.assertTrue(tree.search("oop",2).values.containsAll(listOf("troop", "loop")))
    }

}