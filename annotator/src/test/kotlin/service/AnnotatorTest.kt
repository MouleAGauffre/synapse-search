package service

import io.quarkus.test.junit.QuarkusTest
import java.util.*
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

@QuarkusTest
class AnnotatorTest {

    private val annotator : Annotator = Annotator()

    @Before
    fun setup() {
        annotator.addDictionaryEntries(mapOf(
            UUID.randomUUID().toString() to "computer",
            UUID.randomUUID().toString() to "computer science",
            UUID.randomUUID().toString() to "artificial intelligence",
            UUID.randomUUID().toString() to "science",
            UUID.randomUUID().toString() to "quantum computing",
            UUID.randomUUID().toString() to "distributed intelligence"
        ))
    }

    @Test
    fun shouldAnnotate() =
        assertTrue(annotator.annotate("artificial intelligence is a field of computer science")
            .containsAll(listOf("artificial intelligence", "computer science", "computer", "science")))

}