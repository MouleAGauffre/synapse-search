package resource

import javax.ws.rs.GET
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import service.Annotator

@Path("/")
class AnnotatorResource {

    private val annotator = Annotator()

    @Path("/addDictionaryEntries")
    @POST
    fun addDictionaryEntries(entries: List<DictionaryEntry>) =
        annotator.addDictionaryEntries(entries.associate { it.id to it.entry })

    @Path("/annotate/{text}")
    @GET
    fun annotate(@PathParam("text") text: String): List<String> = annotator.annotate(text)

    data class DictionaryEntry(val id: String, val entry: String)

}