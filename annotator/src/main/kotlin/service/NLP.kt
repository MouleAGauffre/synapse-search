package service

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation
import edu.stanford.nlp.pipeline.Annotation
import edu.stanford.nlp.pipeline.StanfordCoreNLP
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation
import java.util.*
import java.util.stream.Collectors.joining
import java.util.stream.Collectors.toList

/**
 * The multithreaded NLP module for text mining based on CoreNLP.
 */
class NLP {

    private var pipeline: StanfordCoreNLP? = null

    init {
        val pipelineProps = Properties()
        pipelineProps.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,parse")
        pipelineProps.setProperty("threads", Runtime.getRuntime().availableProcessors().toString())
        pipeline = StanfordCoreNLP(pipelineProps)
    }

    fun findNames(text: String, maxNumberOfWords: Int): List<String> {
        val annotation = Annotation(text.toLowerCase())
        pipeline!!.annotate(annotation)
        val tree = annotation.get(SentencesAnnotation::class.java)[0]
            .get(TreeAnnotation::class.java)

        return tree.subTreeList().parallelStream()
            .filter { it.label().toString() in listOf("NN", "NP") }
            .map { it.yieldWords() }
            .filter { it.size <= maxNumberOfWords }
            .map { words ->
                words.parallelStream()
                    .map { it.word() }
                    .collect(joining(" "))
            }
            .distinct()
            .collect(toList())
    }

}