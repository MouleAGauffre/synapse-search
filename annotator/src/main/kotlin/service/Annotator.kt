package service

import org.apache.commons.text.similarity.LevenshteinDistance
import org.eclipse.microprofile.config.inject.ConfigProperty
import kotlin.streams.toList

class Annotator {

    private val nlp: NLP = NLP();
    private val tree: BKTree = BKTree(LevenshteinDistance());

    @ConfigProperty(name = "nlp.max.word.size")
    private val maxWordNumber: Int = System.getenv("NLP_MAX_WORD_SIZE").toInt();

    @ConfigProperty(name = "nlp.edit.distance.tolerance")
    private val tolerance: Int = System.getenv("NLP_EDIT_DISTANCE_TOLERANCE").toInt();

    /**
     * @param (id, label) map
     */
    fun addDictionaryEntries(words: Map<String, String>) = words.forEach { tree.index(it.value, it.value) }

    fun annotate(text: String): List<String> {
        return nlp.findNames(text, maxWordNumber).parallelStream()
            .flatMap { tree.search(it, tolerance).entries.parallelStream() }
            .map { it.key }
            .toList()
    }

}