package service

import org.apache.commons.text.similarity.EditDistance
import kotlin.streams.toList

/**
 * My own BK-tree impl: https://en.wikipedia.org/wiki/BK-tree for fast fuzzy string search :)
 * - Adding new entries is synchronized and single threaded
 * - The lookups are multi-threaded
 */
class BKTree(private val editDistance: EditDistance<Int>) {

    private var root: Node? = null

    private fun addNode(s: String, id: String, d: Int, parent: Node? = root) {
        val newNode = Node(id, s)

        if (root == null) {
            root = newNode
        } else {
            if (parent != null) {
                parent.children[d] = newNode
            }
        }
    }

    @Synchronized
    fun index(s: String, id: String, current: Node? = root) {
        if (root == null) {
            addNode(s, id, 0)
            return
        }

        if (current != null) {
            val d = editDistance.apply(s, current.value)

            val next = current.children[d]
            if (next == null) {
                addNode(s, id, d, current)
                return

            } else {
                index(s, id, next)
            }
        }
    }

    /**
     * @return A map (resultId, resultName)
     */
    fun search(name: String, tolerance: Int, current: Node? = root): Map<String, String> {
        if (current != null) {
            val d = editDistance.apply(name, current.value)
            val min = d - tolerance
            val max = d + tolerance

            val results = hashMapOf<String, String>()

            if (d <= tolerance) {
                results[current.id] = current.value
            }

            results.putAll(current.children.entries.parallelStream()
                .filter { it.key in min..max }
                .flatMap { search(name, tolerance, it.value).entries.parallelStream() }
                .toList()
                .associate { it.key to it.value })

            return results

        } else {
            return hashMapOf()
        }
    }

    data class Node(val id: String, val value: String, val children: HashMap<Int, Node> = hashMapOf())

}